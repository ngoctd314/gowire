# Go Wire

Dependency has expensive tradeoff is the complexity of initializing dependencies

Wire is a code dependency tool that operates without runtime state or reflection. In Wire, dependencies between components are represented as function parameters, encouraging explicit initilization instead of global variables.

