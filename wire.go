//go:build wireinject
// +build wireinject

package main

import "github.com/google/wire"

func InitializeEvent(textMessage, greeterPrefix) Event {
	wire.Build(GetMessage, GetGreeter, GetEvent)
	return Event{}
}
