package main

import (
	"fmt"
)

type Message string
type Greeter struct {
	Message Message
}
type Event struct {
	Greeter Greeter
}

type textMessage string

type greeterPrefix string

func GetMessage(text textMessage) Message {
	return Message(text)
}
func GetGreeter(m Message, prefix greeterPrefix) Greeter {
	fmt.Println("prefix: ", prefix)
	return Greeter{Message: m}
}
func (g Greeter) Greet() Message {
	return g.Message
}
func GetEvent(g Greeter) Event {
	return Event{Greeter: g}
}
func (e Event) Start() {
	msg := e.Greeter.Greet()
	fmt.Println(msg)
}
func main() {
	e := InitializeEvent(textMessage("Hello TDN"), greeterPrefix("3.14"))
	e.Start()
}
